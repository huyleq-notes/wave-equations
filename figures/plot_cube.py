
import matplotlib
matplotlib.use('TkAgg') 
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.lines as lines

fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
#ax.set_aspect(.5)

size=0.5
small=0.02
small2=2*small

left, width = 0,size
bottom, height = 1.-1.6*size,size
right = left + width
top = bottom + height
p=patches.Rectangle((left,bottom),width,height,fill=False,transform=ax.transAxes,clip_on=False)
ax.add_patch(p)

x1=left
y1=bottom+size

x=[x1,x1+0.7*size,x1+1.7*size,x1+size]
y=[y1,y1+0.6*size,y1+0.6*size,y1]
l=lines.Line2D(x,y,c='k')
ax.add_line(l)

for i in range(4):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='k',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

x=[x1,x1+0.7*size,x1+1.7*size]
y=[bottom,bottom+0.6*size,bottom+0.6*size]
l=lines.Line2D(x,y,c='k',linestyle='--')
ax.add_line(l)

for i in range(3):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='k',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

p=patches.Rectangle((left+size-small,bottom-small),small2,small2,fill=True,facecolor='k',transform=ax.transAxes,clip_on=False)
ax.add_patch(p)

x=[left+0.7*size,left+0.7*size]
y=[bottom+0.6*size,bottom+1.6*size]
l=lines.Line2D(x,y,c='k',linestyle='--')
ax.add_line(l)

x=[x1+1.7*size,x1+1.7*size,x1+size]
y=[y1+0.6*size,y1-0.4*size,y1-size]
l=lines.Line2D(x,y,c='k')
ax.add_line(l)

ax.text(0.75*size,1-1.6*size-0.03,"X",horizontalalignment='center',verticalalignment='center',fontsize=15,color='k',transform=ax.transAxes)
ax.text(1.175*size+0.015,1-1.45*size-0.025,"Y",horizontalalignment='center',verticalalignment='center',fontsize=15,color='k',transform=ax.transAxes)
ax.text(1.7*size+0.015,1-0.75*size,"Z",horizontalalignment='center',verticalalignment='center',fontsize=15,color='k',transform=ax.transAxes)

x=[left,left+1.7*size]
y=[bottom+size,bottom+1.6*size]
l=lines.Line2D(x,y,c='k')
ax.add_line(l)

#x=[left+0.7*size,left+size]
#y=[bottom+1.5*size,bottom+size]
#l=lines.Line2D(x,y,c='k')
#ax.add_line(l)

x=[left,left+1.7*size]
y=[bottom,bottom+.6*size]
l=lines.Line2D(x,y,c='k',linestyle='--')
ax.add_line(l)

#x=[left+0.7*size,left+size]
#y=[bottom+.5*size,bottom]
#l=lines.Line2D(x,y,c='k',linestyle='--')
#ax.add_line(l)

#x=[left,left+.7*size]
#y=[bottom+size,bottom+.5*size]
#l=lines.Line2D(x,y,c='k',linestyle='--')
#ax.add_line(l)

#x=[left,left+.7*size]
#y=[bottom,bottom+1.6*size]
#l=lines.Line2D(x,y,c='k',linestyle='--')
#ax.add_line(l)

#x=[left+size,left+1.7*size]
#y=[bottom+size,bottom+.5*size]
#l=lines.Line2D(x,y,c='k')
#ax.add_line(l)

#x=[left+size,left+1.7*size]
#y=[bottom,bottom+1.6*size]
#l=lines.Line2D(x,y,c='k')
#ax.add_line(l)

#x=[left,left+size]
#y=[bottom,bottom+size]
#l=lines.Line2D(x,y,c='k')
#ax.add_line(l)

x=[left,left+size]
y=[bottom+size,bottom]
l=lines.Line2D(x,y,c='k')
ax.add_line(l)

x=[left+0.7*size,left+1.7*size]
y=[bottom+1.6*size,bottom+0.6*size]
l=lines.Line2D(x,y,c='k',linestyle='--')
ax.add_line(l)

#x=[left+0.7*size,left+1.7*size]
#y=[bottom+.5*size,bottom+1.5*size]
#l=lines.Line2D(x,y,c='k',linestyle='--')
#ax.add_line(l)

x=[left+0.7*size,left+size]
y=[bottom+1.6*size,bottom]
l=lines.Line2D(x,y,c='k',linestyle='--')
ax.add_line(l)

x=[left+0.85*size,left+0.85*size]
y=[bottom+1.3*size,bottom+0.3*size]
for i in range(2):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='r',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

pi=patches.Rectangle((left+0.85*size-small,bottom+0.8*size-small),small2,small2,fill=True,facecolor='g',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)

x=[left+0.5*size,left+1.2*size]
y=[bottom+.5*size,bottom+1.1*size]
for i in range(2):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='b',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

x=[left+0.5*size,left+1.2*size,left+1.2*size,left+0.5*size]
y=[bottom+size,bottom+1.6*size,bottom+0.6*size,bottom]
for i in range(4):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='y',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

x=[left,left+0.7*size,left+1.7*size,left+size]
y=[bottom+0.5*size,bottom+1.1*size,bottom+1.1*size,bottom+0.5*size]
for i in range(4):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='c',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

x=[left+0.35*size,left+1.35*size,left+1.35*size,left+0.35*size]
y=[bottom+1.3*size,bottom+1.3*size,bottom+.3*size,bottom+0.3*size]
for i in range(4):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='m',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

xi,yi=left+1.525*size,bottom+0.335*size
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='k',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.08,yi+0.01,r'$v_x,\rho$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='k',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='r',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.08,yi+0.01,r'$v_y,\rho$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='r',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='b',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.08,yi+0.01,r'$v_z,\rho$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='b',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='y',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.11,yi+0.01,r'$\sigma_{ii},c_{ij(i,j\leq 3)}$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='y',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='g',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.085,yi+0.01,r'$\sigma_{yz},c_{44}$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='g',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='c',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.085,yi+0.01,r'$\sigma_{xz},c_{55}$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='c',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi,yi),small2,small2,fill=True,facecolor='m',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.085,yi+0.01,r'$\sigma_{xy},c_{66}$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='m',transform=ax.transAxes)

ax.set_axis_off()

plt.savefig("cube.pdf",bbox_inches='tight',pad_inches=0)
#plt.show()
