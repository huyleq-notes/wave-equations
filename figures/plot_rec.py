
import matplotlib
matplotlib.use('TkAgg') 
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.lines as lines

fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
#ax.set_aspect(.5)

size=0.7
small=0.02
small2=2*small

left, width = 0.,size
bottom, height = 0.2,size
right = left + width
top = bottom + height
p=patches.Rectangle((left,bottom),width,height,fill=False,transform=ax.transAxes,clip_on=False)
ax.add_patch(p)

x1=left
y1=bottom+size

x=[x1,x1+size,x1+size,x1]
y=[y1,y1,bottom,bottom]
for i in range(4):
    pi=patches.Rectangle((x[i]-small,y[i]-small),small2,small2,fill=True,facecolor='k',transform=ax.transAxes,clip_on=False)
    ax.add_patch(pi)

xi,yi=left+.5*size,y1
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='r',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)

yi=bottom
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='r',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)

xi,yi=left,bottom+.5*size
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='b',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)

xi=left+size
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='b',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)

xi,yi=left+0.5*size,bottom+.5*size
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='y',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)

xi,yi=left+size+0.07,bottom+.5*size+0.3
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='k',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.05,yi,r'$v_x,\rho$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='k',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='y',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.05,yi,r'$v_z,\rho$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='y',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='r',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.095,yi-0.01,r'$\sigma_{ii},c_{ij,(i,j\leq 3)}$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='r',transform=ax.transAxes)

yi=yi-1.5*small2
pi=patches.Rectangle((xi-small,yi-small),small2,small2,fill=True,facecolor='b',transform=ax.transAxes,clip_on=False)
ax.add_patch(pi)
ax.text(xi+small+0.06,yi-0.01,r'$\sigma_{xz},c_{44}$',horizontalalignment='center',verticalalignment='center',fontsize=15,color='b',transform=ax.transAxes)

ax.set_axis_off()

plt.savefig("rec.pdf",bbox_inches='tight',pad_inches=0)
#plt.show()
